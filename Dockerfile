FROM maven:3.8.3-openjdk-18 as BUILD

RUN mvn clean package

FROM openjdk:18-jdk

VOLUME /tmp

EXPOSE 8080
EXPOSE 80

ADD target/*.jar app.jar

ENTRYPOINT ["java","-jar","/app.jar"]