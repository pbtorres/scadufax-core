package scadufax.core.resources;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.*;
import scadufax.core.DTO.ErroNegocial;
import scadufax.core.DTO.ResponseDTO;
import scadufax.core.exceptions.ResourceServiceException;
import scadufax.core.exceptions.SuperException;
import scadufax.core.interfaces.PrescriptionInterface;
import scadufax.core.models.persistence.Prescription;

import javax.persistence.PersistenceException;
import javax.ws.rs.core.MediaType;

@RestController
@RequestMapping(value = "/prescription")
@Tag(name = "Receita", description = "Receitas criadas pelo usuário")
public class PrescriptionResource extends BasicResource<Prescription> {

	@Autowired
	private PrescriptionInterface service;

	@PostMapping
	@Operation(summary = "Cadastra um novo item")
	@ApiResponses(value = {
			@ApiResponse(
					responseCode = "201",
					description = "Para casos de sucesso",
					content = @Content(
							mediaType = MediaType.APPLICATION_JSON,
							schema = @Schema(
									implementation = ResponseDTO.class
//									type = SchemaType.DEFAULT
							)
					)
			),
			@ApiResponse(
					responseCode = "400",
					description = "Para casos de erro",
					content = @Content(
							mediaType = MediaType.APPLICATION_JSON,
							schema = @Schema(
									implementation = ErroNegocial.class
//									type = SchemaType.DEFAULT
							)
					)
			),
		}
	)
	public ResponseEntity<ResponseDTO> insert(@RequestBody Prescription obj) throws ResourceServiceException {
		try {
			super.validate(obj);
			return super.created(service.insertOrUpdate(obj));
		} catch (SuperException | PersistenceException | TransactionSystemException e) {
			throw new ResourceServiceException(e);
		}
	}
	
	@GetMapping
	@Operation(summary = "Retorna todos os ítens")
	@ApiResponses(value = {
			@ApiResponse(
					responseCode = "200",
					description = "Para casos de sucesso",
					content = @Content(
							mediaType = MediaType.APPLICATION_JSON,
							schema = @Schema(
									implementation = ResponseDTO.class
//									type = SchemaType.DEFAULT
							)
					)
			),
			@ApiResponse(
					responseCode = "400",
					description = "Para casos de erro",
					content = @Content(
							mediaType = MediaType.APPLICATION_JSON,
							schema = @Schema(
									implementation = ErroNegocial.class
//									type = SchemaType.DEFAULT
							)
					)
			),
		}
	)
	public ResponseEntity<ResponseDTO> findAll() throws ResourceServiceException{
		try {
			return super.ok(service.findAll());
		} catch (SuperException e) {
			throw new ResourceServiceException(e);
		}
	}
	
	@GetMapping(value="/{id}")
	@Operation(summary = "Retorna o item com base no id")
	@ApiResponses(value = {
			@ApiResponse(
					responseCode = "200",
					description = "Para casos de sucesso",
					content = @Content(
							mediaType = MediaType.APPLICATION_JSON,
							schema = @Schema(
									implementation = ResponseDTO.class
//									type = SchemaType.DEFAULT
							)
					)
			),
			@ApiResponse(
					responseCode = "400",
					description = "Para casos de erro",
					content = @Content(
							mediaType = MediaType.APPLICATION_JSON,
							schema = @Schema(
									implementation = ErroNegocial.class
//									type = SchemaType.DEFAULT
							)
					)
			),
	}
	)
	public ResponseEntity<ResponseDTO> findById(@PathVariable Long id) throws ResourceServiceException {
		try {
			return super.ok(service.findById(id));
		} catch (SuperException | PersistenceException e) {
			throw new ResourceServiceException(e);
		}
	}
	
	@PutMapping
	@Operation(summary = "Atualiza o item")
	@ApiResponses(value = {
			@ApiResponse(
					responseCode = "200",
					description = "Para casos de sucesso",
					content = @Content(
							mediaType = MediaType.APPLICATION_JSON,
							schema = @Schema(
									implementation = ResponseDTO.class
//									type = SchemaType.DEFAULT
							)
					)
			),
			@ApiResponse(
					responseCode = "400",
					description = "Para casos de erro",
					content = @Content(
							mediaType = MediaType.APPLICATION_JSON,
							schema = @Schema(
									implementation = ErroNegocial.class
//									type = SchemaType.DEFAULT
							)
					)
			),
			@ApiResponse(
					responseCode = "404",
					description = "Para casos de erro",
					content = @Content(
							mediaType = MediaType.APPLICATION_JSON,
							schema = @Schema(
									implementation = ErroNegocial.class
//									type = SchemaType.DEFAULT
							)
					)
			),
		}
	)
	public ResponseEntity<ResponseDTO> update(@RequestBody Prescription obj) throws ResourceServiceException {
		try {
			Prescription entity = service.insertOrUpdate(obj);
			return super.validate(entity);
		} catch(SuperException | PersistenceException e) {
			throw new ResourceServiceException(e);
		}
	}
	
	@DeleteMapping(value="/{id}")
	@Operation(summary = "Deleta o ítem a partir do id")
	@ApiResponses(value = {
			@ApiResponse(
					responseCode = "200",
					description = "Para casos de sucesso com informação adicional",
					content = @Content(
							mediaType = MediaType.APPLICATION_JSON,
							schema = @Schema(
									implementation = ResponseDTO.class
//									type = SchemaType.DEFAULT
							)
					)
			),
			@ApiResponse(
					responseCode = "204",
					description = "Para casos de sucesso",
					content = @Content(mediaType = MediaType.APPLICATION_JSON)
			),
			@ApiResponse(
					responseCode = "400",
					description = "Para casos de erro",
					content = @Content(
							mediaType = MediaType.APPLICATION_JSON,
							schema = @Schema(
									implementation = ErroNegocial.class
//									type = SchemaType.DEFAULT
							)
					)
			),
			@ApiResponse(
					responseCode = "404",
					description = "Para casos de erro",
					content = @Content(
							mediaType = MediaType.APPLICATION_JSON,
							schema = @Schema(
									implementation = ErroNegocial.class
//									type = SchemaType.DEFAULT
							)
					)
			),
		}
	)
	public ResponseEntity<ResponseDTO> delete(@PathVariable Long id) throws ResourceServiceException {
		try {
			Prescription entity = service.findById(id);
			super.validate(entity);
			service.delete(id);
			return super.ok(entity);
		} catch(SuperException | PersistenceException e) {
			throw new ResourceServiceException(e);
		}
		
	}

}
