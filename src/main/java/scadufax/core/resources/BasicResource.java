package scadufax.core.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import scadufax.core.DTO.ErroNegocial;
import scadufax.core.DTO.ResponseDTO;
import scadufax.core.enums.StatusTransaction;
import scadufax.core.exceptions.ResourceServiceException;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.HashMap;
import java.util.Set;

public abstract class BasicResource<T> {
	
	@Autowired
	private Validator validator;
	
	public ResponseEntity<ResponseDTO> ok(Object object) {
		ResponseDTO responseDTO = new ResponseDTO(
				StatusTransaction.OK.message,
				StatusTransaction.OK, object);
		
	    return ResponseEntity.ok().body(responseDTO);
	}
	
	public ResponseEntity<ResponseDTO> created(Object object) {
	    ResponseDTO responseDTO = new ResponseDTO(
	      StatusTransaction.CREATED.message,
	      StatusTransaction.CREATED,
	      object
	    );
	    return ResponseEntity.created(null).body(responseDTO);
	}
	
	public ResponseEntity<ResponseDTO> accepted(Object object, String message) {
	    ResponseDTO responseDTO = new ResponseDTO(
	      message,
	      StatusTransaction.ACCEPTED,
	      object
	    );
	    return ResponseEntity.accepted().body(responseDTO);
	}

	public ResponseEntity<ResponseDTO> validate(T object) throws ResourceServiceException {
	   
		Set<ConstraintViolation<T>> violations = validator.validate(object);
	    
	    if (violations != null && violations.size() > 0) {
	      HashMap<String, Object> map = new HashMap<>();
	      for (ConstraintViolation<?> v : violations) {
	        map.put(
	          v.getPropertyPath().toString().toLowerCase(),
	          v.getMessage().toLowerCase()
	        );
	      }
	      ErroNegocial erroNegocial = new ErroNegocial(
	        StatusTransaction.BAD_REQUEST,
	        StatusTransaction.BAD_REQUEST.message,
	        map
	      );

	      throw new ResourceServiceException(erroNegocial);
	    }
	    return accepted(object, String.format("%s validado.",
	    			object.getClass().getSimpleName().toUpperCase()));
	}

}
