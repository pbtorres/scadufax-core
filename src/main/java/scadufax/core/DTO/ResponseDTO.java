package scadufax.core.DTO;

import java.io.Serializable;

import scadufax.core.enums.StatusTransaction;

public class ResponseDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String debugMessage;
	private StatusTransaction status;
	private Object object;

	public ResponseDTO(String debugMessage, StatusTransaction status, Object object) {
		this.debugMessage = debugMessage;
		this.status = status;
		this.object = object;
	}
	
	public ResponseDTO(Object object) {
		this.object = object;
	}

	public String getDebugMessage() {
		return debugMessage;
	}

	public void setDebugMessage(String debugMessage) {
		this.debugMessage = debugMessage;
	}

	public StatusTransaction getStatus() {
		return status;
	}

	public void setStatus(StatusTransaction status) {
		this.status = status;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	@Override
	public String toString() {
		return "ResponseDTO [debugMessage=" + debugMessage + 
				", object=" + object + "]";
	}

}
