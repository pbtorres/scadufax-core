package scadufax.core.DTO;

import java.io.Serializable;
import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonFormat;

import scadufax.core.enums.StatusTransaction;

public class ErroNegocial implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonFormat(
			shape = JsonFormat.Shape.STRING,
			pattern = "yyyy-MM-dd@HH:mm:ss.SSSZ",
			timezone = "GMT"
	)
	private Instant timestamp;

	private StatusTransaction status;
	private String message;
	private Object debugMessage;

	public ErroNegocial(
			StatusTransaction status,
			String message, Object debugMessage) {
		this.timestamp = Instant.now();
		this.status = status;
		this.message = message;
		this.debugMessage = debugMessage;
	}

	public ErroNegocial() {
		this.timestamp = Instant.now();
	}

	public Instant getTimestamp() {
		return timestamp;
	}

	public StatusTransaction getStatus() {
		return status;
	}	

	public Object getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getDebugMessage() {
		return debugMessage;
	}

	public void setDebugMessage(Object debugMessage) {
		this.debugMessage = debugMessage;
	}

	@Override
	public String toString() {
		return "ErroNegocial [timestamp=" + timestamp + ", message=" + message + 
				", debugMessage=" + debugMessage + "]";
	}

}
