package scadufax.core.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import scadufax.core.models.persistence.Prescription;

public interface PrescriptionDao extends JpaRepository<Prescription, Long>{

}
