package scadufax.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScadufaxCoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScadufaxCoreApplication.class, args);
	}

}
