package scadufax.core.exceptions;

import scadufax.core.DTO.ErroNegocial;

public class ResourceServiceException extends Exception {

	private static final long serialVersionUID = 1L;

	ErroNegocial erroNegocial;

	public ResourceServiceException(String message) {
		super(message);
	}

	public ResourceServiceException(String message, Exception exception) {
		super(message, exception);
	}

	public ResourceServiceException(Exception exception) {
		super(exception);
	}

	public ResourceServiceException(ErroNegocial erroNegocial) {
		this.erroNegocial = erroNegocial;
	}

	public ResourceServiceException(ErroNegocial erroNegocial, Exception exception) {
		this.erroNegocial = erroNegocial;
	}

	public ErroNegocial getErroNegocial() {
		return this.erroNegocial;
	}

}
