package scadufax.core.exceptions;

import scadufax.core.DTO.ErroNegocial;

public class SuperException extends ResourceServiceException {

	private static final long serialVersionUID = 1L;

	public SuperException(String message) {
		super(message);
	}

	public SuperException(String message, Exception exception) {
		super(message, exception);
	}

	public SuperException(Exception exception) {
		super(exception);
	}

	public SuperException(ErroNegocial erroNegocial) {
		super(erroNegocial);
	}

}
