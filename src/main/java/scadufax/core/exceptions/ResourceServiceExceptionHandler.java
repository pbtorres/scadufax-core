package scadufax.core.exceptions;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import scadufax.core.DTO.ErroNegocial;
import scadufax.core.enums.StatusTransaction;

@ControllerAdvice
public class ResourceServiceExceptionHandler {

	@ExceptionHandler(ResourceServiceException.class)
	public ResponseEntity<ErroNegocial> resourceServiceException(ResourceServiceException exception) {

		String message = exception.getCause() != null && exception.getCause().getMessage() != null
				? exception.getCause().getMessage()
				: exception.getMessage();

		ErroNegocial erro = new ErroNegocial(
				StatusTransaction.BAD_REQUEST,
				message, exception.getMessage());

		if (exception.getErroNegocial() != null) {
			erro = exception.getErroNegocial();
		}

		return ResponseEntity.badRequest().body(erro);

	}

}
