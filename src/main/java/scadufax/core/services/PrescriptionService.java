package scadufax.core.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import scadufax.core.DTO.ErroNegocial;
import scadufax.core.dao.PrescriptionDao;
import scadufax.core.enums.StatusTransaction;
import scadufax.core.exceptions.ResourceServiceException;
import scadufax.core.exceptions.SuperException;
import scadufax.core.interfaces.PrescriptionInterface;
import scadufax.core.models.persistence.Prescription;

import javax.persistence.PersistenceException;
import java.util.List;

@Service
public class PrescriptionService implements PrescriptionInterface {

	@Autowired
	private PrescriptionDao dao;

	ErroNegocial erroNegocial = new ErroNegocial(
			StatusTransaction.PRECONDITION_FAILED,
			"Erro Generico",
			this.getClass());

	@Override
	public Prescription insertOrUpdate(Prescription entity) throws ResourceServiceException {
		try {
			return dao.save(entity);
		} catch (PersistenceException e) {
			erroNegocial.setDebugMessage(
					String.format("%s: %s", 
							getClass().getSimpleName(),
							e.getMessage()));
			throw new ResourceServiceException(erroNegocial);
		}
	}

	@Override
	public List<Prescription> findAll() throws ResourceServiceException {
		try {
			return dao.findAll();
		} catch (PersistenceException e) {
			erroNegocial.setDebugMessage(
					String.format("%s: %s",
					getClass().getSimpleName(),
					e.getMessage()));
			throw new ResourceServiceException(erroNegocial);
		}
	}

	@Override
	public Prescription findById(Long id) throws ResourceServiceException {
		try {
			return dao.findById(id)
					.orElseThrow(() -> new SuperException("Não encontrado."));
		} catch (PersistenceException e) {
			erroNegocial.setDebugMessage(
					String.format("%s: %s",
					getClass().getSimpleName(),
					e.getMessage()));
			throw new SuperException(e);
		}
	}

	@Override
	public void delete(Long id) throws ResourceServiceException {
		try {
			dao.deleteById(id);
		} catch (PersistenceException e) {
			erroNegocial.setDebugMessage(
					String.format("%s: %s",
					getClass().getSimpleName(),
					e.getMessage()));
			throw new SuperException(e);
		}
	}

}
