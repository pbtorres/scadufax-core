package scadufax.core.models.persistence;

import scadufax.core.models.abstracted.BasicEntity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_prescription")
public class Prescription extends BasicEntity {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotNull
	@NotBlank
	private String title;
	
	private String description;
	
	public Prescription() {
		super();
	}

	public Prescription(String title, String description, LocalDateTime createdTimestamp, LocalDateTime timestamp) {
		super(createdTimestamp, timestamp);
		this.title = title;
		this.description = description;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Prescription [id=" + id + 
				", title=" + title + 
				", description=" + description + "]";
	}
	
}
