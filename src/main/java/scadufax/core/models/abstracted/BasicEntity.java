package scadufax.core.models.abstracted;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.time.LocalDateTime;

@MappedSuperclass
public abstract class BasicEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@CreationTimestamp
	@JsonFormat(
			shape = JsonFormat.Shape.STRING,
			pattern = "yyyy-MM-dd@HH:mm:ss.SSSZ",
			locale = "pt_BR"
	)
	@Column(nullable = false, updatable = false)
	@JsonIgnore
	private LocalDateTime createdTimestamp;
	
	@UpdateTimestamp
	@JsonFormat(
		shape = JsonFormat.Shape.STRING,
		pattern = "yyyy-MM-dd@HH:mm:ss.SSSZ",
		locale = "pt_BR"
	)
	@JsonIgnore
	private LocalDateTime timestamp;
	
	public BasicEntity() {
		
	}
	
	public BasicEntity(LocalDateTime createdTimestamp, LocalDateTime timestamp) {
		this.createdTimestamp = createdTimestamp;
		this.timestamp = timestamp;
	}
	
	public LocalDateTime getCreatedTimestamp() {
		return createdTimestamp;
	}
	
	public void setCreatedTimestamp(LocalDateTime createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}
	
	public LocalDateTime getTimestamp() {
		return timestamp;
	}
	
	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public String toString() {
		return "BasicEntity [createdTimestamp=" + createdTimestamp + ", timestamp=" + timestamp + "]";
	}
	
}
