package scadufax.core.config;

import java.time.LocalDateTime;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import scadufax.core.dao.PrescriptionDao;
import scadufax.core.models.persistence.Prescription;

@Configuration
@Profile("test")
public class TestConfig implements CommandLineRunner{
	
	@Autowired
	private PrescriptionDao dao;
	
	@Override
	public void run(String... args) throws Exception {

		Prescription p1 = new Prescription("Prescription 1", "Descrição 1", LocalDateTime.now(), LocalDateTime.now());
		Prescription p2 = new Prescription("Prescription 2", "Descrição 2", LocalDateTime.now(), LocalDateTime.now());
		Prescription p3 = new Prescription("Prescription 3", "Descrição 3", LocalDateTime.now(), LocalDateTime.now());
		
		dao.saveAll(Arrays.asList(p1, p2, p3));
	}

}
