package scadufax.core.interfaces;

import java.util.List;

import scadufax.core.exceptions.ResourceServiceException;
import scadufax.core.models.persistence.Prescription;

public interface PrescriptionInterface {
	
	Prescription insertOrUpdate(Prescription entity) throws ResourceServiceException;
	
	List<Prescription> findAll() throws ResourceServiceException;
	
	Prescription findById(Long id) throws ResourceServiceException;
	
	void delete(Long id) throws ResourceServiceException;
	
}
